/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.peeraya.oxprogram;

import org.junit.jupiter.api.Test;
import static org.junit.jupiter.api.Assertions.*;

/**
 *
 * @author ADMIN
 */
public class OXTest {

    public OXTest() {
    }
    
    // test CheckVerticalOWin()
    @Test
    public void testCheckVerticalPlayerOWinCol1() {
        char table[][] = {{'O', '-', '-'},
                          {'O', '-', '-'},
                          {'O', '-', '-'}};
        char currentPlayer = 'O';
        int col = 1;
        assertEquals(true, OXProgram.checkVertical(table, currentPlayer, col));
    }
    
    @Test
    public void testCheckVerticalPlayerOWinCol2() {
        char table[][] = {{'-', 'O', '-'},
                          {'-', 'O', '-'},
                          {'-', 'O', '-'}};
        char currentPlayer = 'O';
        int col = 2;
        assertEquals(true, OXProgram.checkVertical(table, currentPlayer, col));
    }
    
    @Test
    public void testCheckVerticalPlayerOWinCol3() {
        char table[][] = {{'-', '-', 'O'},
                          {'-', '-', 'O'},
                          {'-', '-', 'O'}};
        char currentPlayer = 'O';
        int col = 3;
        assertEquals(true, OXProgram.checkVertical(table, currentPlayer, col));
    }
    
    @Test
    public void testCheckHorizentalPlayerOWinRow1() {
        char table[][] = {{'O', 'O', 'O'},
                          {'-', '-', '-'},
                          {'-', '-', '-'}};
        char currentPlayer = 'O';
        int row = 1;
        assertEquals(true, OXProgram.checkHorizental(table, currentPlayer, row));
    }
    
    @Test
    public void testCheckHorizentalPlayerOWinRow2() {
        char table[][] = {{'-', '-', '-'},
                          {'O', 'O', 'O'},
                          {'-', '-', '-'}};
        char currentPlayer = 'O';
        int row = 2;
        assertEquals(true, OXProgram.checkHorizental(table, currentPlayer, row));
    }
    
    @Test
    public void testCheckHorizentalPlayerOWinRow3() {
        char table[][] = {{'-', '-', '-'},
                          {'-', '-', '-'},
                          {'O', 'O', 'O'}};
        char currentPlayer = 'O';
        int row = 3;
        assertEquals(true, OXProgram.checkHorizental(table, currentPlayer, row));
    }
    
    @Test
    public void testCheckVerticalPlayerXWinCol1() {
        char table[][] = {{'X', '-', '-'},
                          {'X', '-', '-'},
                          {'X', '-', '-'}};
        char currentPlayer = 'X';
        int col = 1;
        assertEquals(true, OXProgram.checkVertical(table, currentPlayer, col));
    }
    
    @Test
    public void testCheckVerticalPlayerXWinCol2() {
        char table[][] = {{'-', 'X', '-'},
                          {'-', 'X', '-'},
                          {'-', 'X', '-'}};
        char currentPlayer = 'X';
        int col = 2;
        assertEquals(true, OXProgram.checkVertical(table, currentPlayer, col));
    }
    
    @Test
    public void testCheckVerticalPlayerXWinCol3() {
        char table[][] = {{'-', '-', 'X'},
                          {'-', '-', 'X'},
                          {'-', '-', 'X'}};
        char currentPlayer = 'X';
        int col = 3;
        assertEquals(true, OXProgram.checkVertical(table, currentPlayer, col));
    }
    
    @Test
    public void testCheckHorizentalPlayerXWinRow1() {
        char table[][] = {{'X', 'X', 'X'},
                          {'-', '-', '-'},
                          {'-', '-', '-'}};
        char currentPlayer = 'X';
        int row = 1;
        assertEquals(true, OXProgram.checkHorizental(table, currentPlayer, row));
    }
    
    @Test
    public void testCheckHorizentalPlayerXWinRow2() {
        char table[][] = {{'-', '-', '-'},
                          {'X', 'X', 'X'},
                          {'-', '-', '-'}};
        char currentPlayer = 'X';
        int row = 2;
        assertEquals(true, OXProgram.checkHorizental(table, currentPlayer, row));
    }
    
    @Test
    public void testCheckHorizentalPlayerXWinRow3() {
        char table[][] = {{'-', '-', '-'},
                          {'-', '-', '-'},
                          {'X', 'X', 'X'}};
        char currentPlayer = 'X';
        int row = 3;
        assertEquals(true, OXProgram.checkHorizental(table, currentPlayer, row));
    }
    
    @Test
    public void testCheckX1PlayerOWin() {
        char table[][] = {{'O', '-', '-'},
                          {'-', 'O', '-'},
                          {'-', '-', 'O'}};
        char currentPlayer = 'O';
        assertEquals(true, OXProgram.checkX1(table, currentPlayer));
    }
    
    @Test
    public void testCheckX2PlayerOWin() {
        char table[][] = {{'-', '-', 'O'},
                          {'-', 'O', '-'},
                          {'O', '-', '-'}};
        char currentPlayer = 'O';
        assertEquals(true, OXProgram.checkX2(table, currentPlayer));
    }
    
    @Test
    public void testCheckX1PlayerXWin() {
        char table[][] = {{'X', '-', '-'},
                          {'-', 'X', '-'},
                          {'-', '-', 'X'}};
        char currentPlayer = 'X';
        assertEquals(true, OXProgram.checkX1(table, currentPlayer));
    }
    
    @Test
    public void testCheckX2PlayerXWin() {
        char table[][] = {{'-', '-', 'X'},
                          {'-', 'X', '-'},
                          {'X', '-', '-'}};
        char currentPlayer = 'X';
        assertEquals(true, OXProgram.checkX2(table, currentPlayer));
    }
    
}
