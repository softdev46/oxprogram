/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.peeraya.oxprogram;

import org.junit.jupiter.api.Test;
import static org.junit.jupiter.api.Assertions.*;

/**
 *
 * @author ADMIN
 */
public class TDDTest {

    public TDDTest() {

    }

    // add (int a, int b) >> int
    // add (1, 2) >> 3
    @Test
    public void testAdd_1_2is3() {
        assertEquals(3, Example.add(1, 2));
    }

    // add (3, 4) >> 7
    @Test
    public void testAdd_3_4is7() {
        assertEquals(7, Example.add(3, 4));
    }
    
    // add (20, 22) >> 42
    @Test
    public void testAdd_20_22is42() {
        assertEquals(42, Example.add(20, 22));
    }
    
    // p:paper, s:scissors, h:hammer
    // chup (char player1, char player2) >> 'p', 'p' >> "draw"
    @Test
    public void testChup_p1_p_p2_p_is_draw(){
        assertEquals("draw", Example.chup('p', 'p'));
    }
    
    // chup (char player1, char player2) >> 'h', 'h' >> "draw"
    @Test
    public void testChup_p1_h_p2_h_is_draw(){
        assertEquals("draw", Example.chup('h', 'h'));
    }
    
    // chup (char player1, char player2) >> 's', 's' >> "draw"
    @Test
    public void testChup_p1_s_p2_s_is_draw(){
        assertEquals("draw", Example.chup('s', 's'));
    }
    
    // chup (char player1, char player2) >> 's', 'p' >> "p1"
    @Test
    public void testChup_p1_s_p2_p_is_p1(){
        assertEquals("p1", Example.chup('s', 'p'));
    }
    
    // chup (char player1, char player2) >> 'p', 'h' >> "p1"
    @Test
    public void testChup_p1_p_p2_h_is_p1(){
        assertEquals("p1", Example.chup('p', 'h'));
    }
    
    // chup (char player1, char player2) >> 'h', 's' >> "p1"
    @Test
    public void testChup_p1_h_p2_s_is_p1(){
        assertEquals("p1", Example.chup('h', 's'));
    }
    
    // chup (char player1, char player2) >> 's', 'h' >> "p2"
    @Test
    public void testChup_p1_s_p2_h_is_p2(){
        assertEquals("p2", Example.chup('s', 'h'));
    }
    
    // chup (char player1, char player2) >> 'h', 'p' >> "p2"
    @Test
    public void testChup_p1_h_p2_p_is_p2(){
        assertEquals("p2", Example.chup('h', 'p'));
    }
    
    // chup (char player1, char player2) >> 'p', 's' >> "p2"
    @Test
    public void testChup_p1_p_p2_s_is_p2(){
        assertEquals("p2", Example.chup('p', 's'));
    }

}
