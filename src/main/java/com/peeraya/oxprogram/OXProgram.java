/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.peeraya.oxprogram;

import java.util.Scanner;
import javax.swing.plaf.TableUI;

/**
 *
 * @author ADMIN
 */
public class OXProgram {

    static char table[][] = {{'-', '-', '-'}, {'-', '-', '-'}, {'-', '-', '-'}};
    static char currentPlayer = 'O';
    static int row, col;
    static Scanner kb = new Scanner(System.in);
    static boolean finish = false;
    static int count = 0;

    public static void main(String[] args) {
        showWelcome();
        while (true) {
            showTable();
            showTurn();
            inputRowCol();
            process();
            if (finish) {
                break;
            }
        }
    }

    public static void showWelcome() {
        System.out.println("Welcome to OX Game");
    }

    public static void showTable() {
        for (int r = 0; r < table.length; r++) {
            for (int c = 0; c < table[r].length; c++) {
                System.out.print(table[r][c]);
            }
            System.out.println("");
        }
    }

    public static void showTurn() {
        System.out.println("Turn " + currentPlayer);
    }

    public static void inputRowCol() {
        System.out.println("Please input row, col: ");
        row = kb.nextInt();
        col = kb.nextInt();
    }

    public static void process() {
        if (setTable()) {
            if (checkWin(table, currentPlayer, row, col)) {
                finish = true;
                showWin();
                return;
            }
            if (checkDraw(count)) {
                finish = true;
                showDraw();
                return;
            }
            count++;
            switchPlayer();
        }
    }

    private static void showWin() {
        showTable();
        System.out.println(">>> " + currentPlayer + " Win <<<");
    }

    public static void switchPlayer() {
        if (currentPlayer == 'O') {
            currentPlayer = 'X';
        } else {
            currentPlayer = 'O';
        }
    }

    public static boolean setTable() {
        table[row - 1][col - 1] = currentPlayer;
        return true;
    }

    private static boolean checkWin(char[][] table, char currentPlayer, int row, int col) {
        if (checkVertical(table, currentPlayer, col)) {
            return true;
        } else if (checkHorizental(table, currentPlayer, row)) {
            return true;
        } else if (checkX(table, currentPlayer)) {
            return true;
        }
        return false;
    }

    public static boolean checkVertical(char[][] table, char currentPlayer, int col) {
        for (int r=0; r<table.length; r++) {
            if (table[r][col-1]!=currentPlayer) {
                return false;
            }
        }
        return true;
    }

    public static boolean checkHorizental(char[][] table, char currentPlayer, int row) {
        for (int c=0; c<table.length; c++) {
            if (table[row-1][c]!=currentPlayer) {
                return false;
            }
        }
        return true;
    }

    public static boolean checkX(char[][] table, char currentPlayer) {
        if (checkX1(table, currentPlayer)) {
            return true;
        } else if (checkX2(table, currentPlayer)) {
            return true;
        }
        return false;
    }

    public static boolean checkX1(char[][] table, char currentPlayer) { // (1,1),(2,2),(3,3)
        for (int i=0; i<table.length; i++) {
            if (table[i][i]!=currentPlayer) {
                return false;
            }
        }
        return true;
    }

    public static boolean checkX2(char[][] table, char currentPlayer) { // (1,3),(2,2),(3,1)
        for (int i=0; i<table.length; i++) {
            if (table[i][2-i]!=currentPlayer) {
                return false;
            }
        }
        return true;
    }

    public static boolean checkDraw(int count) {
        if (count == 8) {
            return true;
        }
        return false;
    }

    private static void showDraw() {
        System.out.println(">>> Draw <<<");
    }
}
